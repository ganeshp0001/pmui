var keyMirror = require('keymirror');

module.exports = keyMirror({
  USER_CREATE: null,
  USER_SAVE: null,
  USER_LOGIN: null
});
