var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	defaults: {
		name: '',
		createdOn: '',
		website: ''
	},

	url: function(){
		return "192.168.33.10/project";
	}
});