'use strict';

var Reflux = require('reflux');

var TaskActions = Reflux.createActions([
	'loadTaskDetail',
  'loadProjectTasks',
  'loadUserTasks',
  'createTask',
  'cancelTaskEdit',
  'saveTask',
  'removeTask'
]);

module.exports = TaskActions;
