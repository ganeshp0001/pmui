'use strict';

var Reflux = require('reflux');

var UserActions = Reflux.createActions([
  'createUser',
  'saveUser',
  'updateUser',
  'addUser',
  'removeRemove',
  'deleteUser'
]);

module.exports = UserActions;
