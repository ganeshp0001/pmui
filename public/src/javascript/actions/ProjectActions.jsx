'use strict';

var Reflux = require('reflux');

var ProjectActions = Reflux.createActions([
  'createProject',
  'editProject',
  'cancelProjectEdit',
  'saveProject',
  'removeProject'
]);

module.exports = ProjectActions;
