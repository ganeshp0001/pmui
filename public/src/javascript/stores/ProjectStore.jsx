'use strict';

var Reflux = require('reflux'),
    _ = require('lodash'),
    ProjectActions = require('../actions/ProjectActions.jsx');
var toastr = require('toastr');
    require('toastr/toastr.css');

var _data = {
	projects: [
	{ title: 'Simility', notification_count: 10 },
	{ title: 'Xytext', notification_count: 15 },
	{ title: 'Femfessional', notification_count: 1 },
	{ title: 'StartupTech', notification_count: 9 },
	{ title: 'RasikKatta', notification_count: 10 }
	]
},
    _defer = null;

_.forEach(_data.projects, function(prj, index){
	prj.id = Date.now() + index;
});
var ProjectStore = Reflux.createStore({
  listenables:[ProjectActions],
  fetchData: function(){
  	//TODO: fetch data from server and then trigger
  	var that = this;
  	setTimeout(function(){
	  	that.trigger(_data);
  	}, 1000);
  },
  onCreateProject: function(){
  	_data.isEditProject = true;
    _data.currentProject = {};
  	this.trigger(_data);
  },
  onCancelProjectEdit: function(){
  	_data.isEditProject = false;
  	this.trigger(_data);
  },
  onEditProject: function(data){
    var index = _.findIndex(_data.projects, {id: data.id});
    _data.isEditProject = true;
    _data.currentProject = _data.projects[index];
    this.trigger(_data);
  },
  onSaveProject: function(data){
  	_data.isEditProject = false;
  	_data.projects.push({id: Date.now(), title: data.title, notification_count: 0 });
  	toastr.success(data.title + ' Project created', '<b>Success!</b>');
  	this.trigger(_data);
  },
  onRemoveProject: function(){

  }
});

module.exports = ProjectStore;
