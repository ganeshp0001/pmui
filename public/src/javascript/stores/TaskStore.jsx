'use strict';

var Reflux = require('reflux'),
    _ = require('lodash'),
    TaskActions = require('../actions/TaskActions.jsx');
var toastr = require('toastr');
    require('toastr/toastr.css');

var _data = {
  tasks: [
  { title: 'Simility' },
  { title: 'Xytext' },
  { title: 'Femfessional' },
  { title: 'StartupTech'},
  { title: 'Simility' },
  { title: 'Xytext' },
  { title: 'Femfessional' },
  { title: 'StartupTech'},
  { title: 'Simility' },
  { title: 'Xytext' },
  { title: 'Femfessional' },
  { title: 'StartupTech'},
  { title: 'RasikKatta'}
  ]
},
    _defer = null;

_.forEach(_data.tasks, function(prj){
  prj.id = Date.now();
});
var TaskStore = Reflux.createStore({
  listenables:[TaskActions],
  onLoadProjectTasks: function(data){
    //TODO: fetch data from server and then trigger
    var that = this;
    _.forEach(_data.tasks, function(task){ task.title = task.title + ' : ' + data.id});
      console.debug('RET TASKS', _data);
      _data.id = data.id;
      that.trigger(_data);
    }
});

module.exports = TaskStore;
