var React = require('react');
var Router = require('react-router'); // or var Router = ReactRouter; in browsers
var ProjectsApp = require('./../components/ProjectsApp');
// IP: 192.126.83.97
// username: saurabh
// password: uE67$jhT
//https://www.dropbox.com/sh/oyor9wa64o4lglz/AAA99NB2E9-e91OeSh-RNug_a?dl=0
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
  render: function () {
    return (
      <div>
        <RouteHandler/>
      </div>
    );
  }
});

var routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="projects" handler={ProjectsApp}/>
    <DefaultRoute handler={ProjectsApp}/>
  </Route>
);
// 
Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById("page-content"));
});