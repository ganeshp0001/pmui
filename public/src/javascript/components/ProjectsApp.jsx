var React = require('react'),
	Reflux = require('reflux'),
	_ = require('lodash');
var ProjectList = require('./projectcomponents/ProjectList'),
    ProjectForm = require('./projectcomponents/ProjectForm'),
    TaskList = require('./projectcomponents/TaskList'),
    ProjectStore = require('../stores/ProjectStore');

module.exports = React.createClass({
	mixins: [Reflux.connect(ProjectStore)],
	getInitialState: function(){
		return {};
	},
	componentWillMount: function(){
		ProjectStore.fetchData();
	},
	render: function(){
		if(!_.has(this.state, 'projects')){
			console.debug(this.state);
			return (<div />);
		}

		var middleColumn, rightColumn, defaultProject = 0;
		if(this.state.isEditProject){
			middleColumn = (
					<ProjectForm data={this.state.currentProject} />
				);
		}else{
      if(this.state.projects.length > 0) defaultProject = this.state.projects[0].id;
			middleColumn = (
          <TaskList defaultId={defaultProject}  />
				);
		}

		if(this.state.showUsers){
			rightColumn = (
				<div className="col-md-2">Online users</div>
				);
		}else{
			rightColumn = (
				<div className="col-md-2">Hide Online users</div>
				);
		}
		return (
			<div className="content container-fluid">
				<div className="row-fluid">
				<ProjectList projects={this.state.projects} />
				{middleColumn}
				{rightColumn}
				</div>

				<div className="row-fluid">
				<div className="col-md-12">
				<div className="quote">&copy; CodeSphere Solutions</div>
				</div>
				</div>
			</div>
			);
	}

});
