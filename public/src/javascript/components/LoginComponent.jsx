var React = require('react');
var UserActions = require('../actions/User.Actions');
var UserStore = require('../stores/User.Store');

function getUserState(){
	return {
			isLoggedIn: UserStore.getUser()
		};	
}

module.exports = React.createClass({
	getInitialState: function(){
		return getUserState();
	},
	_login: function(){
		UserActions.login({
			email: "abc@xyz.com",
			Password: "mypwd134"
		});
	},

	componentDidMount: function() {
		UserStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		UserStore.removeChangeListener(this._onChange);
	},

	_onChange: function(){
		this.setState(getUserState());
	},

	render: function(){
		var randomColor = Math.floor(Math.random()*16777215).toString(16);
		var style1 = {
			margin: "10px"
		};
		var style2 = {
			borderLeft: "0px",
			borderRight: "0px"
		};
		var style3 = {
			backgroundColor: randomColor
		}
		if(this.state.isLoggedIn){
			window.location.href = "#projects";
			// return (
			// <div className="content">
			// 	<div className="title">Logged In</div>
			// 	<div className="quote">&copy; CodeSphere Solutions</div>
			// </div>
			// 	)
		}
		else{
		return (
			<div className="content" style={style3}>
					<div className="login-container">
						<div className="bounceInLeft animated">
							<div className="input-group" style={style1}>
							  <span className="input-group-addon">Email</span>
							  <input type="text" className="form-control" placeholder="Email" />
							  <span className="input-group-addon" style={style2} >Password</span>
							  <input type="text" className="form-control" aria-describedby="btn-login" placeholder="Password" />
							  <span className="input-group-addon btn btn-primary" id="btn-login" onClick={this._login}>Login</span>
							</div>
						</div>
					</div>
				<div className="title">Project Management 5</div>
				<div className="quote">&copy; CodeSphere Solutions</div>
			</div>
			);
		}
	}

});