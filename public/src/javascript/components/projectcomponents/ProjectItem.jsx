'use strict';

var React = require('react'),
    Reflux = require('reflux');
var toastr = require('toastr');
    require('toastr/toastr.css');

var TaskActions = require('../../actions/TaskActions.jsx'),
    ProjectActions = require('../../actions/ProjectActions.jsx'),
    TaskStore = require('../../stores/TaskStore');

module.exports = React.createClass({
  mixins: [Reflux.connect(TaskStore)],
	_onEdit: function(){
		toastr.info("Project id: " + this.props.project.id );
    ProjectActions.editProject({id: this.props.project.id});
	},
	_loadTasks: function(){
    TaskActions.loadProjectTasks({id: this.props.project.id});
	},
	render: function(){
    var className = (this.state.id === this.props.project.id)? 'project-item selected': 'project-item';
		return (
			<div className={className}>
        <div className="row-fluid">
            <div className="col-md-9"  onClick={this._loadTasks}>
      				{this.props.project.title} : {this.props.project.notification_count}
            </div>
            <div className="col-md-3">
      				<button className="btn btn-info pull-right" onClick={this._onEdit}>E</button>
            </div>
        </div>
				<div className="clearfix"></div>
			</div>
			);
	}

});
