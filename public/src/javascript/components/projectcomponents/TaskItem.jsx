'use strict';

var React = require('react');

module.exports = React.createClass({
	_loadTaskDetail: function(){

	},
	render: function(){
		return (
			<div className="task-item" onClick={this._loadTaskDetail}>
        {_.trunc(this.props.task.title, 80)}
			</div>
			);
	}

});
