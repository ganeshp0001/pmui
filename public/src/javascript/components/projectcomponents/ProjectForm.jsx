'use strict';

var React = require('react'),
	$ = require('jquery'),
    ProjectActions = require('../../actions/ProjectActions.jsx');

module.exports = React.createClass({
	_cancel: function(){
		ProjectActions.cancelProjectEdit();
	},
	_save: function(){
		var title = $("#title").val(),
			desc = $("#desc").val(),
			users = $('#users').val(),
			data = {
				title: title,
				desc: desc,
				users: users
			};

		ProjectActions.saveProject(data);
	},
	render: function(){
		return (
			<div  className="col-md-7">
				<div className="project-form">
					<div>
						<label>Title</label>
					</div>
					<div>
						<input id="title" placeholder="Title" value={this.props.data.title} />
					</div>
					<div>
						<label>Description</label>
					</div>
					<div>
						<textarea id="desc"></textarea>
					</div>
					<div>
						<label>Users</label>
					</div>
					<div>
						<input id="users" placeholder="Users" />
					</div>
					<div>
						<button onClick={this._save}>Save</button>
						<button onClick={this._cancel}>Cancel</button>
					</div>
				</div>
			</div>
			);
	}
});
