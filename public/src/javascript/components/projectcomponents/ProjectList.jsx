var React = require('react'),
	_ = require('lodash');

var ProjectItem = require('./ProjectItem'),
    ProjectActions = require('../../actions/ProjectActions.jsx');

module.exports = React.createClass({
	_addProject: function(){
		ProjectActions.createProject();
	},
	render: function(){
		var projects = [],
			project = {};
		_.forEach(this.props.projects, function(prj){
			project = (<ProjectItem  project={prj} />)
			projects.push(project);
		});
		return (
			<div className="col-md-3">
        <div className="row-fluid">
            <div className="col-md-7">
              <input placeholder="Search Project" />
            </div>
            <div className="col-md-5">
              <div className="btn-group">
                <div className="btn btn-success" onClick={this._addTask}> S </div>
                <div className="btn btn-primary" onClick={this._addProject}> +P </div>
              </div>
            </div>
        </div>
        <div className="clearfix"></div>
			<div className="project-list">
				{projects}
			</div>
			</div>
			);
	}
});
