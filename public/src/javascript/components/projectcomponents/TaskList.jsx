var React = require('react'),
    Reflux = require('reflux'),
	_ = require('lodash');

var TaskItem = require('./TaskItem'),
    TaskActions = require('../../actions/TaskActions.jsx'),
    TaskStore = require('../../stores/TaskStore');

module.exports = React.createClass({
  mixins: [Reflux.connect(TaskStore)],
	_addTask: function(){
		ProjectActions.createProject();
	},
  componentWillMount: function(){
    TaskActions.loadProjectTasks({id: this.props.defaultId});
  },
	render: function(){
		var tasks = [],
			task = {};
		_.forEach(this.state.tasks, function(task){
			task = (<TaskItem  task={task} />)
			tasks.push(task);
		});
		return (
			<div className="col-md-7">
        <div className="row-fluid">
            <div className="col-md-8">
              <input placeholder="Search Task" />
            </div>
            <div className="col-md-4">
              <div className="btn-group">
                <div className="btn btn-success" onClick={this._addTask}> S </div>
                <div className="btn btn-warning" onClick={this._addTask}> +T </div>
              </div>
            </div>
        </div>
        <div className="clearfix"></div>
				<div className="task-list">
					{tasks}
				</div>
			</div>
			);
	}
});
