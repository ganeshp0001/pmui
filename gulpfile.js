var fs = require("fs");
var gulp        = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var browserSync = require('browser-sync').create();
var history = require('connect-history-api-fallback');
var reload      = browserSync.reload;
var webpack = require('gulp-webpack');

//TODO: Need to add reflux instead of flux :) (Thanks to Ben Wu)

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        port: 5666,
        server: {
            baseDir: "./public"
        }
    });

	gulp.watch("public/src/javascript/**", ['webpack']);
	gulp.watch("public/src/style/sass/*.scss", ['sass']);
	gulp.watch("public/*.html").on('change', reload);
});

gulp.task("webpack", function() {
    return gulp.src('public/scr/javascript/app.jsx')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('./public/js'))
        .pipe(browserSync.reload({stream:true}));
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("public/src/style/sass/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.reload({stream:true}));
});
gulp.task('default', ['sass','webpack', 'browser-sync']);